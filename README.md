---
editor_options: 
  markdown: 
    wrap: 80
---

Figure 4: Relative expression of human RNA-binding proteins (RBPs) in the human
heart cell atlas. Combined single cell and single nuclei RNA-Seq data from cells
of the human heart [1] were aggregated to cell types, normalized, scaled and
filtered for expressed RNAs of human RBPs [2] and visualized as heatmap.
Classical RBPs [3], RBPs with classical protein domains [2] and RBPs of interest
in heart were annotated. Data sources for the human heart cell atlas is provided
as column annotation. Source data and code is available at [4].

[1] Litviňuková, M., Talavera-López, C., Maatz, H. *et al.* Cells of the adult
human heart. *Nature* **588**, 466--472 (2020).
<https://doi.org/10.1038/s41586-020-2797-4>

[2] Gebauer, F., Schwarzl, T., Valcárcel, J. et al. RNA-binding proteins in
human genetic disease. Nat Rev Genet 22, 185--198 (2021).
<https://doi.org/10.1038/s41576-020-00302-y*>

[3] Gerstberger, S., Hafner, M. & Tuschl, T. A census of human RNA-binding
proteins. *Nat Rev Genet* **15**, 829--845 (2014).
<https://doi.org/10.1038/nrg3813>

[4] Schwarzl, Thomas. (2023). Relative expression of human RNA-binding proteins
(RBPs) in the human heart cell atlas. Zenodo.
<https://doi.org/10.5281/zenodo.8112712>

Heatmap of RBPs in human heart cells

340292 cells of atrial cardiomyocytes, ventricular cardiomyocytes, fibroblasts, 
endothelial, lymphoid and myeloid cells from combined single cell and single
nuclei RNA-Seq data from human heart cell atlas [1] were aggregated, 
variance stabilizing transformation normalized [2] and filtered for at least 30 
percent relative expression in any of the cell types. Heatmap of scaled
normalized expression of 3004 mRNAs of human RBPs [3] was annotated with 
1309 classical RBPs [4] and 702 RBPs with classical RNA-binding domains (RBDs) 
using ComplexHeatmap package [5]. mRNAs of RBPs with interest to the human 
heart were annotated with labels. Cell sources for the combined sequencing data
id displayed on top [1]. All source data and code is available at [6], raw
data for the heamap is available as Supp. Table XXX.


[1] Litviňuková, M., Talavera-López, C., Maatz, H. *et al.* Cells of the adult
human heart. *Nature* **588**, 466--472 (2020).
<https://doi.org/10.1038/s41586-020-2797-4>
[2] Love, M.I., Huber, W. and Anders, S., 2014. Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2. Genome biology, 15(12), pp.1-21.
[3] Gebauer, F., Schwarzl, T., Valcárcel, J. et al. RNA-binding proteins in
human genetic disease. Nat Rev Genet 22, 185--198 (2021).
<https://doi.org/10.1038/s41576-020-00302-y*>
[4] Gerstberger, S., Hafner, M. & Tuschl, T. A census of human RNA-binding
proteins. *Nat Rev Genet* **15**, 829--845 (2014).
<https://doi.org/10.1038/nrg3813>
[5] Gu, Z., Eils, R. and Schlesner, M., 2016. Complex heatmaps reveal patterns 
and correlations in multidimensional genomic data. Bioinformatics, 32(18), pp.2847-2849.
[6] Schwarzl, Thomas. (2023). Relative expression of human RNA-binding proteins
(RBPs) in the human heart cell atlas. Zenodo.
<https://doi.org/10.5281/zenodo.8112712>

